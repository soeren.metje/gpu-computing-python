# GPU Computing with Python

This project took place at the _University of Göttingen_ in scope of the seminar [_Newest Trends in High-Performance Data Analytics SS22_](https://hps.vi4io.org/teaching/summer_term_2022/nthpda).
It contains a collection of Jupyter notebooks to try Python frameworks for GPU computing.
I also compared the computing performance with cpu computing frameworks like Numpy.

## Frameworks

I had a look at following frameworks:
- Numba
- CuPy
- Pytorch
- Numpy

## Benchmark
As a result, the diagram shows the times of a matrix multiplication benchmark.

<img src="./plots/plot_benchmark-float32.jpg" width="800" >


## Setup 
For setting up runtime and dependencies, please have a look at the frameworks' documentation.